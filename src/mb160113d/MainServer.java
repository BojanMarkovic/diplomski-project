package mb160113d;

import javafx.application.Application;
import javafx.stage.Stage;
import mb160113d.gui.ServerWindow;

public class MainServer extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            new ServerWindow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}