package mb160113d.services;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mb160113d.exceptions.GuiException;

import java.io.File;
import java.util.Optional;

public class GuiService {

    public static final int TEXTAREA_WIDTH = 500;
    private static final int GAP_10 = 10;
    private static final int GAP_5 = 5;
    private static final int FIELD_WIDTH = 300;
    private static final String NORMAL_STYLE = "-fx-background-color: dodgerblue; -fx-text-fill: white";
    private static final String CLICKED_STYLE = "-fx-background-color: blue; -fx-text-fill: white";
    private static final String COLOR_TEXT = "-fx-background-color: white";
    private static final String DEFAULT_DIR = "user.dir";

    public void setStage(String title, Stage stage) throws GuiException {
        try {
            stage.setTitle(title);
            stage.setResizable(false);
        } catch (Exception e) {
            throw new GuiException("Something happened while setting stage.", e);
        }
    }

    public FlowPane generateFlowPane() throws GuiException {
        try {
            FlowPane flowPane = new FlowPane();
            flowPane.setPadding(new Insets(GAP_10, GAP_10, GAP_10, GAP_10));
            flowPane.setVgap(GAP_5);
            flowPane.setHgap(GAP_10);
            flowPane.setAlignment(Pos.BASELINE_CENTER);
            flowPane.setOrientation(Orientation.HORIZONTAL);
            return flowPane;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating flow pane.", e);
        }
    }

    public HBox generateTextFieldHBox(String text) throws GuiException {
        try {
            TextField textField = new TextField();
            textField.setMinWidth(FIELD_WIDTH);
            Label label = new Label(text);
            label.setMinWidth(FIELD_WIDTH);
            HBox temp = new HBox(label, textField);
            temp.setSpacing(GAP_5);
            return temp;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating textfield.", e);
        }
    }

    public HBox generatePasswordFieldHBox(String text) throws GuiException {
        try {
            PasswordField passwordField = new PasswordField();
            passwordField.setMinWidth(FIELD_WIDTH);
            Label label = new Label(text);
            label.setMinWidth(FIELD_WIDTH);
            HBox temp = new HBox(label, passwordField);
            temp.setSpacing(GAP_5);
            return temp;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating password field.", e);
        }
    }

    public TextField getTextFieldHBox(HBox hBox) throws GuiException {
        try {
            if (hBox != null && hBox.getChildren().get(1) instanceof TextField) {
                return (TextField) hBox.getChildren().get(1);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new GuiException("Something happened while getting textfield.", e);
        }
    }

    public HBox generateComboBoxHBox(ObservableList<String> observableList, String text) throws GuiException {
        try {
            ComboBox<String> comboBox = new ComboBox<>(observableList);
            if (!observableList.isEmpty()) {
                comboBox.setValue(observableList.get(0));
            }
            comboBox.setMinWidth(FIELD_WIDTH);
            comboBox.setStyle(COLOR_TEXT);
            Label label = new Label(text);
            label.setMinWidth(FIELD_WIDTH);
            HBox temp = new HBox(label, comboBox);
            temp.setSpacing(GAP_5);
            return temp;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating combobox.", e);
        }
    }

    public String getValueComboBoxHBox(HBox comboBoxHBox) throws GuiException {
        try {
            if (comboBoxHBox != null && comboBoxHBox.getChildren().get(1) instanceof ComboBox) {
                //noinspection unchecked
                return ((ComboBox<String>) comboBoxHBox.getChildren().get(1)).getValue();
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new GuiException("Something happened while getting value from combobox.", e);
        }
    }

    public HBox generateFileDirChooser(String text, String fileType, boolean isDirectory, Stage stage) throws GuiException {
        try {
            TextField textField = new TextField();
            textField.setMinWidth(FIELD_WIDTH);
            textField.setEditable(false);
            textField.setText(System.getProperty(DEFAULT_DIR));
            Button button = generateButton(text, e -> {
                File file;
                if (isDirectory) {
                    DirectoryChooser directoryChooser = new DirectoryChooser();
                    directoryChooser.setInitialDirectory(new File(System.getProperty(DEFAULT_DIR)));
                    file = directoryChooser.showDialog(stage);
                } else {
                    String temp = "*." + fileType;
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setInitialDirectory(new File(System.getProperty(DEFAULT_DIR)));
                    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(fileType, temp));
                    fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter(fileType, temp));
                    file = fileChooser.showOpenDialog(stage);
                }
                if (file != null) {
                    textField.setText(file.getAbsolutePath());
                }
            });
            HBox temp = new HBox(button, textField);
            temp.setSpacing(GAP_5);
            return temp;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating file chooser.", e);
        }
    }

    public Button generateButton(String text, EventHandler<ActionEvent> event) throws GuiException {
        try {
            Button button = new Button(text);
            button.setMinWidth(FIELD_WIDTH);
            button.setOnAction(event);
            setColorButton(button);
            return button;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating button.", e);
        }
    }

    private void setColorButton(Button button) throws GuiException {
        try {
            button.setStyle(NORMAL_STYLE);
            button.setOnMousePressed(e -> button.setStyle(CLICKED_STYLE));
            button.setOnMouseReleased(e -> button.setStyle(NORMAL_STYLE));
        } catch (Exception e) {
            throw new GuiException("Something happened while setting color of the button.", e);
        }
    }

    public Optional<ButtonType> generateAlert(Alert.AlertType type, String title, String headerText, String contentText)
            throws GuiException {
        try {
            Alert alert = new Alert(type);
            alert.setTitle(title);
            alert.setHeaderText(headerText);
            alert.setContentText(contentText);
            alert.setResizable(false);
            alert.setWidth(FIELD_WIDTH);
            alert.setHeight(FIELD_WIDTH);
            if (type == Alert.AlertType.CONFIRMATION) {
                Button temp = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);
                setColorButton(temp);
            }
            Button temp2 = (Button) alert.getDialogPane().lookupButton(ButtonType.OK);
            setColorButton(temp2);
            if (type == Alert.AlertType.CONFIRMATION || type == Alert.AlertType.INFORMATION) {
                return alert.showAndWait();
            } else {
                alert.show();
            }
            return Optional.empty();
        } catch (Exception e) {
            throw new GuiException("Something happened while generating alert.", e);
        }
    }

    public TextArea generateTextArea() throws GuiException {
        try {
            TextArea textArea = new TextArea();
            textArea.setMinSize(TEXTAREA_WIDTH, TEXTAREA_WIDTH);
            textArea.setEditable(false);
            ScrollPane scrollPane = new ScrollPane();
            scrollPane.setContent(textArea);
            scrollPane.vvalueProperty().bind(textArea.heightProperty());

            textArea.textProperty().addListener((observable, oldValue, newValue) -> textArea.setScrollTop(Double.MIN_VALUE));
            return textArea;
        } catch (Exception e) {
            throw new GuiException("Something happened while generating textarea.", e);
        }
    }
}
