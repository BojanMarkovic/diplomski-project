package mb160113d.services;

import javafx.concurrent.Task;
import javafx.scene.control.TextArea;
import mb160113d.enums.SSLIntConstants;
import mb160113d.enums.SSLStringConstants;
import mb160113d.exceptions.ServerException;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.Callable;

@SuppressWarnings("InfiniteLoopStatement")
public class ServerService {

    private static final String SPLITTER = "##";
    private final TextArea textArea;
    private final CryptoService cryptoService = new CryptoService();
    private final DatabaseService databaseService = new DatabaseService();
    private Task<Void> generateCertificateTask;
    private Task<Void> testCertificateTask;
    private Task<Void> revokeCertificateTask;
    private SSLServerSocket serverSocket1;
    private SSLServerSocket serverSocket2;
    private SSLServerSocket serverSocket3;

    public ServerService(TextArea textArea) {
        this.textArea = textArea;
    }

    @SuppressWarnings("CatchMayIgnoreException")
    public void stopServer() throws ServerException {
        try {
            try {
                if ((serverSocket1 != null) && (!serverSocket1.isClosed())) {
                    serverSocket1.close();
                }
            } catch (Exception e) {
            }
            try {
                if ((serverSocket2 != null) && (!serverSocket2.isClosed())) {
                    serverSocket2.close();
                }
            } catch (Exception e) {
            }
            try {
                if ((serverSocket3 != null) && (!serverSocket3.isClosed())) {
                    serverSocket3.close();
                }
            } catch (Exception e) {
            }
            if ((serverSocket1 != null) && (serverSocket2 != null) &&
                    (serverSocket3 != null) && ((!serverSocket1.isClosed()) ||
                    (!serverSocket2.isClosed()) || (!serverSocket3.isClosed()))) {
                textArea.appendText("Server stopped!\n");
            } else {
                textArea.appendText("Server not started!\n");
            }
        } catch (Exception e) {
            textArea.appendText("Error happened while stopping server:" + e.getMessage());
            throw new ServerException("Error happened while stopping server.", e);
        }
    }

    public void startServer() throws ServerException {
        try {
            if ((generateCertificateTask == null) || (testCertificateTask == null) ||
                    (revokeCertificateTask == null)) {
                generateCertificateTask = new MyTask<>(() -> {
                    System.setProperty(SSLStringConstants.KEYSTORE_PROPERTY.txt, SSLStringConstants.KEYSTORE.txt);
                    System.setProperty(SSLStringConstants.KEYSTORE_PASSWORD_PROPERTY.txt, SSLStringConstants.KEYSTORE_PASSWORD.txt);

                    serverSocket1 = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(SSLIntConstants.PORT_CERTIFICATE.num);
                    serverSocket1.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                    while (true) {
                        try (Socket socket = serverSocket1.accept();
                             DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
                             DataInputStream reader = new DataInputStream(socket.getInputStream())) {

                            String csr = reader.readUTF();
                            addToTextArea("Received: " + csr);
                            String[] strings = reader.readUTF().split(SPLITTER);
                            String user = strings[0];
                            String password = strings[1];
                            try {
                                if (checkIfUserOk(user, password)) {
                                    String certificate = cryptoService.signCSR(csr, SSLStringConstants.KEYSTORE.txt, SSLStringConstants.KEYSTORE_PASSWORD.txt);

                                    addToTextArea("Made: " + certificate);
                                    writer.writeUTF(certificate);
                                } else {
                                    addToTextArea("Username or password is incorrect");
                                    writer.writeUTF("Username or password is incorrect");
                                }
                                writer.flush();
                            } catch (Exception e) {
                                addToTextArea(e.getMessage());
                                writer.writeUTF(e.getMessage());
                                writer.flush();
                            }
                        }
                    }
                });

                testCertificateTask = new MyTask<>(() -> {
                    System.setProperty(SSLStringConstants.KEYSTORE_PROPERTY.txt, SSLStringConstants.KEYSTORE.txt);
                    System.setProperty(SSLStringConstants.KEYSTORE_PASSWORD_PROPERTY.txt, SSLStringConstants.KEYSTORE_PASSWORD.txt);

                    serverSocket2 = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(SSLIntConstants.PORT_OCSP.num);
                    serverSocket2.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                    while (true) {
                        try (Socket socket = serverSocket2.accept();
                             DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
                             DataInputStream reader = new DataInputStream(socket.getInputStream())) {

                            String certificate = reader.readUTF();
                            addToTextArea("Received: " + certificate);
                            try {
                                String valid = cryptoService.checkIfCertificateRevoked(certificate);

                                addToTextArea("Certificate is: " + valid);
                                writer.writeUTF(valid);
                                writer.flush();
                            } catch (Exception e) {
                                addToTextArea(e.getMessage());
                                writer.writeUTF(e.getMessage());
                                writer.flush();
                            }
                        }
                    }
                });

                revokeCertificateTask = new MyTask<>(() -> {
                    System.setProperty(SSLStringConstants.KEYSTORE_PROPERTY.txt, SSLStringConstants.KEYSTORE.txt);
                    System.setProperty(SSLStringConstants.KEYSTORE_PASSWORD_PROPERTY.txt, SSLStringConstants.KEYSTORE_PASSWORD.txt);

                    serverSocket3 = (SSLServerSocket) SSLServerSocketFactory.getDefault().createServerSocket(SSLIntConstants.PORT_REVOKE.num);
                    serverSocket3.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                    while (true) {
                        try (Socket socket = serverSocket3.accept();
                             DataOutputStream writer = new DataOutputStream(socket.getOutputStream());
                             DataInputStream reader = new DataInputStream(socket.getInputStream())) {

                            String[] strings1 = reader.readUTF().split(SPLITTER);
                            String certificate = strings1[0];
                            String reason = strings1[1];
                            addToTextArea("Received: " + certificate + "\n" + "Reason: " + reason);
                            String[] strings2 = reader.readUTF().split(SPLITTER);
                            String user = strings2[0];
                            String password = strings2[1];
                            try {
                                if (checkIfUserOk(user, password)) {
                                    String temp = cryptoService.revokeCertificate(certificate, reason);
                                    addToTextArea(temp);
                                    writer.writeUTF(temp);
                                } else {
                                    addToTextArea("Username or password is incorrect");
                                    writer.writeUTF("Username or password is incorrect");
                                }
                                writer.flush();
                            } catch (Exception e) {
                                addToTextArea(e.getMessage());
                                writer.writeUTF(e.getMessage());
                                writer.flush();
                            }
                        }
                    }
                });

                Thread thread = new Thread(generateCertificateTask);
                thread.setDaemon(true);
                thread.start();

                Thread thread2 = new Thread(testCertificateTask);
                thread2.setDaemon(true);
                thread2.start();

                Thread thread3 = new Thread(revokeCertificateTask);
                thread3.setDaemon(true);
                thread3.start();
                addToTextArea("Server started!");
            } else {
                addToTextArea("Server already started!");
            }
        } catch (Exception e) {
            textArea.appendText("Error happened while stopping server:" + e.getMessage());
            throw new ServerException("Error happened while starting server.", e);
        }
    }

    private void addToTextArea(String text) {
        javafx.application.Platform.runLater(() -> textArea.appendText(text + "\n"));
    }

    private boolean checkIfUserOk(String user, String password) throws ServerException {
        try {
            return databaseService.checkUsernamePassword(user, password);
        } catch (Exception e) {
            throw new ServerException("Username and password check failed.", e);
        }
    }

    private class MyTask<Void> extends Task<Void> {

        private final Callable<Void> functionToCall;

        public MyTask(Callable<Void> functionToCall) {
            this.functionToCall = functionToCall;
        }

        @Override
        protected Void call() throws Exception {
            functionToCall.call();
            return null;
        }

        @Override
        protected void failed() {
            super.failed();
            Throwable throwable = this.getException();
            if (!(throwable instanceof SocketException)) {
                textArea.appendText("Error happened: " + throwable.getMessage());
            }
            throwable.printStackTrace();
            generateCertificateTask = null;
            testCertificateTask = null;
            try {
                serverSocket1.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                serverSocket2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
