package mb160113d.services;

import mb160113d.exceptions.DatabaseException;
import org.bouncycastle.asn1.x509.CRLReason;

import java.sql.*;

public class DatabaseService {
    private static final String URL = "jdbc:sqlite:server.db";
    private static final String REPLACE_STRING = "CRLReason: ";

    public boolean checkUsernamePassword(String username, String password) throws DatabaseException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Users WHERE Username=? AND Password = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DatabaseException("Can't check your username and password.", e);
        } catch (Exception e) {
            throw new DatabaseException("Something happened.", e);
        }
    }

    public boolean checkIfSerialExists(long serial) throws DatabaseException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Certificates WHERE SerialNumber=?");
            preparedStatement.setLong(1, serial);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException e) {
            throw new DatabaseException("Something happened while checking if serialnumber exists.", e);
        } catch (Exception e) {
            throw new DatabaseException("Something happened.", e);
        }
    }

    public String getReasonForRevocationIfRevoked(long serial) throws DatabaseException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT Reason, Date FROM CRL WHERE SerialNumber=? ");
            preparedStatement.setLong(1, serial);
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next() ? CRLReason.lookup(resultSet.getInt(1)).toString() + "\nDate: " + new Date(resultSet.getLong(2)) : null;
        } catch (SQLException e) {
            throw new DatabaseException("Error while checking if certificate is revoked.", e);
        } catch (Exception e) {
            throw new DatabaseException("Something happened.", e);
        }
    }

    public void addNewCertificateToList(long serial, String pemCertificate) throws DatabaseException {
        try (Connection connection = DriverManager.getConnection(URL)) {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Certificates VALUES(?, ?)");
            preparedStatement.setLong(1, serial);
            preparedStatement.setString(2, pemCertificate);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DatabaseException("Error while checking adding new certificate to database.", e);
        } catch (Exception e) {
            throw new DatabaseException("Something happened.", e);
        }
    }

    public String revokeCertificate(long serial, String reason) throws DatabaseException {
        String reasonIfRevoked = getReasonForRevocationIfRevoked(serial);
        if (reasonIfRevoked == null) {
            try (Connection connection = DriverManager.getConnection(URL)) {
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO CRL VALUES(?, ?, ?)");
                preparedStatement.setLong(1, serial);
                preparedStatement.setInt(2, getCRLReasonID(reason));
                preparedStatement.setLong(3, System.currentTimeMillis());
                preparedStatement.executeUpdate();
                return "Certificate revoked - Reason: " + reason;
            } catch (SQLException e) {
                throw new DatabaseException("Can't revoke certificate - can't insert into the database.", e);
            } catch (Exception e) {
                throw new DatabaseException("Something happened.", e);
            }
        } else if (reasonIfRevoked.contains(CRLReason.lookup(CRLReason.certificateHold).toString())) {
            try (Connection connection = DriverManager.getConnection(URL)) {
                if (reason.equals(CRLReason.lookup(CRLReason.removeFromCRL).toString().replace(REPLACE_STRING, ""))) {
                    PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM CRL WHERE SerialNumber = ?");
                    preparedStatement.setLong(1, serial);
                    preparedStatement.executeUpdate();
                    return "Certificate reinstated - Reason: " + reason;
                } else {
                    PreparedStatement preparedStatement = connection.prepareStatement("UPDATE CRL SET Reason =?, Date=? WHERE SerialNumber = ?");
                    preparedStatement.setInt(1, getCRLReasonID(reason));
                    preparedStatement.setLong(2, System.currentTimeMillis());
                    preparedStatement.setLong(3, serial);
                    preparedStatement.executeUpdate();
                    return "Certificate revoked - Reason: " + reason;
                }
            } catch (SQLException e) {
                throw new DatabaseException("Can't revoke/reinstate certificate - can't update the database.", e);
            } catch (Exception e) {
                throw new DatabaseException("Something happened.", e);
            }
        }
        return "Certificate is already revoked.";
    }

    private int getCRLReasonID(String reason) throws DatabaseException {
        switch (reason) {
            case "unspecified":
                return CRLReason.unspecified;
            case "keyCompromise":
                return CRLReason.keyCompromise;
            case "cACompromise":
                return CRLReason.cACompromise;
            case "affiliationChanged":
                return CRLReason.affiliationChanged;
            case "superseded":
                return CRLReason.superseded;
            case "cessationOfOperation":
                return CRLReason.cessationOfOperation;
            case "certificateHold":
                return CRLReason.certificateHold;
            case "removeFromCRL":
                return CRLReason.removeFromCRL;
            case "privilegeWithdrawn":
                return CRLReason.privilegeWithdrawn;
            case "aACompromise":
                return CRLReason.aACompromise;
            default:
                throw new DatabaseException("CRL Reason doesn't exist");
        }
    }
}
