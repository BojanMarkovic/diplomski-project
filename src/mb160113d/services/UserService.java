package mb160113d.services;

import mb160113d.enums.CertificateStringConstants;
import mb160113d.enums.SSLIntConstants;
import mb160113d.enums.SSLStringConstants;
import mb160113d.exceptions.UserException;
import org.bouncycastle.asn1.x500.X500Name;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;

public class UserService {
    private static final String SPLITTER = "##";
    private static final String CERTIFICATE_NAME = "/Certificate." + CertificateStringConstants.CERTIFICATE_FILE_TYPE.txt;
    private final CryptoService cryptoService;

    public UserService() throws UserException {
        try {
            this.cryptoService = new CryptoService();
            System.setProperty(SSLStringConstants.TRUSTSTORE_PROPERTY.txt, SSLStringConstants.TRUSTSTORE.txt);
            System.setProperty(SSLStringConstants.TRUSTSTORE_PASSWORD_PROPERTY.txt, SSLStringConstants.TRUSTSTORE_PASSWORD.txt);
        } catch (Exception e) {
            throw new UserException("Something happened while creating user service.", e);
        }
    }

    /*
                    try {
                        for (String crlURL : crlURLs) {
                            URL url = new URL(crlURL);
                            X509CRL x509CRL;
                            try (InputStream crlStream = url.openStream()) {
                                CertificateFactory cf = CertificateFactory.getInstance(CertificateStringConstants.CERTIFICATE_TYPE.txt);
                                x509CRL = (X509CRL) cf.generateCRL(crlStream);
                            }
                            if (x509CRL.isRevoked(x509Certificate)) {
                                return "Certificate is revoked";
                            }
                        }
                        return "Certificate is valid";
                    } catch (Exception e) {
                        throw new UserException("Can't check if certificate is revoked, failed trying to connect to Internet.", e);
                    }
     */
    public String connectAndTestCertificate(String pathToCertificate) throws UserException {
        try {
            String[] certificates = openCertificate(pathToCertificate);
            X509Certificate x509Certificate = cryptoService.getX509Certificate(certificates[0]);

            String url = cryptoService.getOCSPUrl(x509Certificate);
            if (url.equals(CertificateStringConstants.OCSP_URI.txt)) {
                try (SSLSocket sslSocket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(SSLStringConstants.HOST.txt,
                        SSLIntConstants.PORT_OCSP.num)) {
                    try (DataOutputStream writer = new DataOutputStream(sslSocket.getOutputStream());
                         DataInputStream reader = new DataInputStream(sslSocket.getInputStream())) {

                        sslSocket.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                        sslSocket.startHandshake();
                        writer.writeUTF(certificates[0]);
                        writer.flush();
                        return reader.readUTF();
                    }
                } catch (Exception e) {
                    throw new UserException("Failed connecting to the server for check if certificate is revoked.", e);
                }
            } else {
                if (certificates.length < 2) {
                    throw new UserException("Inputted certificate doesn't contain issuer.");
                }
                X509Certificate issuerX509Certificate = cryptoService.getX509Certificate(certificates[1]);
                return cryptoService.getOCSPResponse(x509Certificate, issuerX509Certificate);
            }
        } catch (Exception e) {
            throw new UserException("Something happened.", e);
        }
    }

    public String[] openCertificate(String pathToCertificate) throws UserException {
        try {
            String file = Files.readString(Paths.get(pathToCertificate));
            if (file.split(CertificateStringConstants.CERTIFICATE_HEADER.txt, -1).length - 1 > 1) {
                String[] strings = file.split(CertificateStringConstants.CERTIFICATE_END.txt);
                return new String[]{strings[0] += CertificateStringConstants.CERTIFICATE_END.txt,
                        strings[1] += CertificateStringConstants.CERTIFICATE_END.txt};
            } else {
                return new String[]{file};
            }
        } catch (Exception e) {
            throw new UserException("Can't read a certificate file: " + pathToCertificate, e);
        }
    }

    public String connectAndSendCertificateRequest(String keyAlgorithm, int keySize, String signatureAlgorithm,
                                                   X500Name subject, String username, String password,
                                                   String privateKeyPassword, String path) throws UserException {
        try {
            this.cryptoService.createKeyPair(keyAlgorithm, keySize);
            String csr = this.cryptoService.generateCSR(signatureAlgorithm, subject);

            try (SSLSocket sslSocket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(SSLStringConstants.HOST.txt,
                    SSLIntConstants.PORT_CERTIFICATE.num)) {
                try (DataOutputStream writer = new DataOutputStream(sslSocket.getOutputStream());
                     DataInputStream reader = new DataInputStream(sslSocket.getInputStream())) {

                    sslSocket.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                    sslSocket.startHandshake();
                    writer.writeUTF(csr);
                    writer.flush();
                    writer.writeUTF(username + SPLITTER + password);
                    writer.flush();
                    String certificate = reader.readUTF();
                    try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path + CERTIFICATE_NAME))) {
                        bufferedWriter.write(certificate);
                    } catch (Exception e) {
                        throw new UserException("Error while writing a certificate.", e);
                    }
                    cryptoService.generateEncryptedPrivateKey(privateKeyPassword, path);
                    return certificate;
                }
            } catch (Exception e) {
                throw new UserException("Failed connecting to the server for sending CSR.", e);
            }
        } catch (Exception e) {
            throw new UserException("Something happened.", e);
        }
    }

    public String connectAndSendCertificateRevoke(String pathToCertificate, String reason,
                                                  String username, String password) throws UserException {
        try {
            String certificate = openCertificate(pathToCertificate)[0];

            try (SSLSocket sslSocket = (SSLSocket) SSLSocketFactory.getDefault().createSocket(SSLStringConstants.HOST.txt,
                    SSLIntConstants.PORT_REVOKE.num)) {
                try (DataOutputStream writer = new DataOutputStream(sslSocket.getOutputStream());
                     DataInputStream reader = new DataInputStream(sslSocket.getInputStream())) {

                    sslSocket.setEnabledProtocols(new String[]{SSLStringConstants.TLS_VERSION.txt});
                    sslSocket.startHandshake();
                    writer.writeUTF(certificate + SPLITTER + reason);
                    writer.flush();
                    writer.writeUTF(username + SPLITTER + password);
                    writer.flush();
                    return reader.readUTF();
                }
            } catch (Exception e) {
                throw new UserException("Failed connecting to the server for revoking certificate.", e);
            }
        } catch (Exception e) {
            throw new UserException("Something happened.", e);
        }
    }
}
