package mb160113d.services;

import mb160113d.enums.CertificateStringConstants;
import mb160113d.exceptions.CryptoException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.MiscPEMGenerator;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.PKCS8Generator;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.openssl.jcajce.JcaPKCS8Generator;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8EncryptorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.OutputEncryptor;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemObjectGenerator;
import org.bouncycastle.util.io.pem.PemWriter;

import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Date;

public class CryptoService {
    private static final BigInteger RSA_EXPONENT = BigInteger.valueOf(65537);
    private static final Long YEAR_IN_MS = 30L * 365L * 24L * 60L * 60L * 1000L;
    private static final String ALGORITHM_NAME = "RSA";
    private static final String PRIVATE_KEY_FILE = "/PrivateKey.key";
    private static final String HTTP_REQUEST_TYPE = "Content-Type";
    private static final String HTTP_REQUEST_PROPERTY = "application/ocsp-request";
    private static final String HTTP_REQUEST_TYPE2 = "Accept";
    private static final String HTTP_REQUEST_PROPERTY2 = "application/ocsp-response";
    private final DatabaseService databaseService = new DatabaseService();
    private KeyPair keyPair;

    public String revokeCertificate(String certificate, String reason) throws CryptoException {
        try {
            X509Certificate x509Certificate = getX509Certificate(certificate);
            return databaseService.revokeCertificate(x509Certificate.getSerialNumber().longValue(), reason);
        } catch (Exception e) {
            throw new CryptoException("Error while revoking certificate.", e);
        }
    }

    private OCSPReq generateOCSPRequest(X509Certificate x509Certificate, X509Certificate issuerX509Certificate) throws CryptoException {
        try {
            DigestCalculatorProvider digestCalculatorProvider = (new JcaDigestCalculatorProviderBuilder()).build();
            CertificateID certificateID = new CertificateID(digestCalculatorProvider.get(CertificateID.HASH_SHA1),
                    new X509CertificateHolder(issuerX509Certificate.getEncoded()), x509Certificate.getSerialNumber());

            OCSPReqBuilder ocspReqBuilder = new OCSPReqBuilder();
            ocspReqBuilder.addRequest(certificateID);
            byte[] nonce = new byte[8];
            new SecureRandom().nextBytes(nonce);
            Extension extension = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, false, new DEROctetString(nonce));
            ocspReqBuilder.setRequestExtensions(new Extensions(extension));
            return ocspReqBuilder.build();
        } catch (Exception e) {
            throw new CryptoException("Error while creating OCSP request.", e);
        }
    }

    public String getOCSPResponse(X509Certificate x509Certificate, X509Certificate issuerX509Certificate) throws CryptoException {
        try {
            String url = getOCSPUrl(x509Certificate);
            if (url == null) {
                throw new CryptoException("OCSP url unknown");
            }
            byte[] ocspReqData = generateOCSPRequest(x509Certificate, issuerX509Certificate).getEncoded();

            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            try {
                httpURLConnection.setRequestProperty(HTTP_REQUEST_TYPE, HTTP_REQUEST_PROPERTY);
                httpURLConnection.setRequestProperty(HTTP_REQUEST_TYPE2, HTTP_REQUEST_PROPERTY2);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setUseCaches(false);
                try (OutputStream out = httpURLConnection.getOutputStream()) {
                    out.write(ocspReqData);
                    out.flush();
                }
                OCSPResp ocspResp = new OCSPResp(httpURLConnection.getInputStream().readAllBytes());
                String ocspResponse = "OCSP request was unsuccessful ";
                switch (ocspResp.getStatus()) {
                    case OCSPResp.SUCCESSFUL:
                        return interpretOCSPResponse(ocspResp, x509Certificate);
                    case OCSPResp.MALFORMED_REQUEST:
                        return ocspResponse + "Reason: Malformed request";
                    case OCSPResp.INTERNAL_ERROR:
                        return ocspResponse + "Reason: Internal error";
                    case OCSPResp.TRY_LATER:
                        return ocspResponse + "Reason: Try later";
                    case OCSPResp.SIG_REQUIRED:
                        return ocspResponse + "Reason: Signature required";
                    case OCSPResp.UNAUTHORIZED:
                        return ocspResponse + "Reason: Unauthorized";
                    default:
                        return ocspResponse;
                }
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }
        } catch (Exception e) {
            throw new CryptoException("Error while trying to get OCSP response.", e);
        }
    }

    private String interpretOCSPResponse(OCSPResp ocspResp, X509Certificate certificate) throws CryptoException {
        try {
            Object ocspResponseObject = ocspResp.getResponseObject();
            if (!(ocspResponseObject instanceof BasicOCSPResp)) {
                return "Unexpected OCSP response object: " + ocspResponseObject;
            }
            BasicOCSPResp basicOcspResponse = (BasicOCSPResp) ocspResp.getResponseObject();

            for (SingleResp singleResponse : basicOcspResponse.getResponses()) {
                if (singleResponse.getCertID().getSerialNumber().equals(certificate.getSerialNumber())) {
                    Object certStatus = singleResponse.getCertStatus();

                    if (CertificateStatus.GOOD == certStatus) {
                        return "OCSP success. \nCertificate is valid.";
                    } else if (certStatus instanceof RevokedStatus) {
                        return "OCSP success. \nCertificate is revoked.";
                    } else {
                        return "OCSP success. \nCertificate status is unknown.";
                    }
                }
            }
            return "OCSP response is not valid";
        } catch (Exception e) {
            throw new CryptoException("Error happened while interpreting OCSP response.", e);
        }
    }

    public String getOCSPUrl(X509Certificate x509Certificate) throws CryptoException {
        try {
            byte[] authAccessExtensionValue = x509Certificate.getExtensionValue(Extension.authorityInfoAccess.getId());
            if (authAccessExtensionValue == null) {
                return null;
            }

            ASN1InputStream asn1InputStream = new ASN1InputStream(new ByteArrayInputStream(authAccessExtensionValue));
            byte[] authAccessExtensionOctets = ((DEROctetString) asn1InputStream.readObject()).getOctets();
            ASN1InputStream asn1InputStream2 = new ASN1InputStream(new ByteArrayInputStream(authAccessExtensionOctets));
            AuthorityInformationAccess authorityInformationAccess = AuthorityInformationAccess.getInstance(asn1InputStream2.readObject());

            for (AccessDescription accessDescription : authorityInformationAccess.getAccessDescriptions()) {
                if (accessDescription.getAccessMethod() != null && accessDescription.getAccessMethod().getId().equals(OCSPObjectIdentifiers.id_pkix_ocsp.getId())) {
                    return DERIA5String.getInstance(accessDescription.getAccessLocation().getName()).getString();
                }
            }
            return null;
        } catch (Exception e) {
            throw new CryptoException("Error happened while getting Authority Info Access from certificate.", e);
        }
    }

    public X509Certificate getX509Certificate(String certificate) throws CryptoException {
        try {
            return new JcaX509CertificateConverter()
                    .getCertificate((X509CertificateHolder) generateObjectFromPEMString(certificate));
        } catch (Exception e) {
            throw new CryptoException("Error while getting certificate.", e);
        }
    }

    public String checkIfCertificateRevoked(String certificate) throws CryptoException {
        try {
            X509Certificate x509Certificate = getX509Certificate(certificate);
            String revoked = databaseService.getReasonForRevocationIfRevoked(x509Certificate.getSerialNumber().longValue());
            if (revoked == null) return "Certificate not revoked.";
            return "Certificate revoked - " + revoked;
        } catch (Exception e) {
            throw new CryptoException("Error while checking if certificate revoked.", e);
        }
    }

    public void createKeyPair(String keyAlgorithm, int keySize) throws CryptoException {
        try {
            keyAlgorithm = keyAlgorithm.toUpperCase();
            AlgorithmParameterSpec algorithmParameterSpec;
            if (ALGORITHM_NAME.equals(keyAlgorithm)) {
                algorithmParameterSpec = new RSAKeyGenParameterSpec(keySize, RSA_EXPONENT);
            } else {
                throw new CryptoException("No key algorithm.");
            }
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyAlgorithm);
            keyPairGenerator.initialize(algorithmParameterSpec, new SecureRandom());
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (Exception e) {
            throw new CryptoException("Error happened while creating key pair.", e);
        }
    }

    public String generateCSR(String signerAlgorithm, X500Name subject) throws CryptoException {
        try {
            ContentSigner contentSigner = new JcaContentSignerBuilder(signerAlgorithm).build(keyPair.getPrivate());

            PKCS10CertificationRequestBuilder certificationRequestBuilder = new JcaPKCS10CertificationRequestBuilder(subject, keyPair.getPublic());
            PKCS10CertificationRequest certificationRequest = certificationRequestBuilder.build(contentSigner);
            PemObjectGenerator pemObjectGenerator = new MiscPEMGenerator(certificationRequest);

            return generatePEMFormat(pemObjectGenerator);
        } catch (Exception e) {
            throw new CryptoException("Error happened while generating CSR.", e);
        }
    }

    public String signCSR(String inputCSR, String keystoreFile, String password) throws CryptoException {
        try {
            PrivateKey caPrivateKey = getPrivateKeyFromKeyStore(keystoreFile, password);
            PKCS10CertificationRequest certificationRequest = (PKCS10CertificationRequest) generateObjectFromPEMString(inputCSR);

            AlgorithmIdentifier signatureAlgorithm = certificationRequest.getSignatureAlgorithm();
            AlgorithmIdentifier digestAlgorithm = new DefaultDigestAlgorithmIdentifierFinder().find(signatureAlgorithm);
            AsymmetricKeyParameter keyParameter = PrivateKeyFactory.createKey(caPrivateKey.getEncoded());

            long serial;
            do {
                serial = Math.abs((new SecureRandom()).nextInt());
            } while (databaseService.checkIfSerialExists(serial));

            X509v3CertificateBuilder certificateBuilder = new X509v3CertificateBuilder(
                    new X500Name(CertificateStringConstants.ISSUER_SUBJECT.txt),
                    new BigInteger(String.valueOf(serial)),
                    new Date(System.currentTimeMillis()),
                    new Date(System.currentTimeMillis() + YEAR_IN_MS),
                    certificationRequest.getSubject(), certificationRequest.getSubjectPublicKeyInfo());
            certificateBuilder.addExtension(generateAuthorityInfoAccessExtension());

            ContentSigner contentSigner = new BcRSAContentSignerBuilder(signatureAlgorithm, digestAlgorithm).build(keyParameter);
            X509CertificateHolder certificateHolder = certificateBuilder.build(contentSigner);

            CertificateFactory certificateFactory = CertificateFactory.getInstance(CertificateStringConstants.CERTIFICATE_TYPE.txt,
                    CertificateStringConstants.PROVIDER.txt);
            X509Certificate certificate;
            try (InputStream inputStream = new ByteArrayInputStream(certificateHolder.toASN1Structure().getEncoded())) {
                certificate = (X509Certificate) certificateFactory.generateCertificate(inputStream);
            }

            String pemFormat = generatePEMFormat(certificate);
            databaseService.addNewCertificateToList(serial, pemFormat);
            return pemFormat;
        } catch (Exception e) {
            throw new CryptoException("Error happened while signing CSR.", e);
        }
    }

    public X500Name getX500NameCertificate(String certificate) throws CryptoException {
        try {
            X509Certificate x509Certificate = getX509Certificate(certificate);
            return new X500Name(x509Certificate.getSubjectX500Principal().getName());
        } catch (Exception e) {
            throw new CryptoException("Error happened while getting X500Name from certificate.", e);
        }
    }

    public Object generateObjectFromPEMString(String pemString) throws CryptoException {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            ByteArrayInputStream pemStream = new ByteArrayInputStream(pemString.getBytes(StandardCharsets.UTF_8));

            Reader pemReader = new BufferedReader(new InputStreamReader(pemStream));
            PEMParser pemParser = new PEMParser(pemReader);
            return pemParser.readObject();
        } catch (Exception e) {
            throw new CryptoException("Error happened reading PEM String.", e);
        }
    }

    public void generateEncryptedPrivateKey(String password, String filePath) throws CryptoException {
        try (PemWriter pemWriter = new PemWriter(new FileWriter(filePath + PRIVATE_KEY_FILE))) {
            Security.addProvider(new BouncyCastleProvider());
            JceOpenSSLPKCS8EncryptorBuilder encryptorsBuilder = new JceOpenSSLPKCS8EncryptorBuilder(PKCS8Generator.AES_256_CBC);
            encryptorsBuilder.setRandom(new SecureRandom());
            encryptorsBuilder.setPasssword(password.toCharArray());
            OutputEncryptor outputEncryptor = encryptorsBuilder.build();
            JcaPKCS8Generator jcaGenerator = new JcaPKCS8Generator(keyPair.getPrivate(), outputEncryptor);
            PemObject pemObject = jcaGenerator.generate();
            pemWriter.writeObject(pemObject);
        } catch (Exception e) {
            throw new CryptoException("Error happened while writing and encrypting private key.", e);
        }
    }

    private String generatePEMFormat(Object object) throws CryptoException {
        try {
            StringWriter stringWriter = new StringWriter();
            try (PemWriter pemWriter = new PemWriter(stringWriter)) {
                PemObjectGenerator pemGenerator = new JcaMiscPEMGenerator(object);
                pemWriter.writeObject(pemGenerator);
            }
            return stringWriter.toString();
        } catch (Exception e) {
            throw new CryptoException("Error happened while generating PEM String.", e);
        }
    }

    private Extension generateAuthorityInfoAccessExtension() throws CryptoException {
        try {
            GeneralName generalName = new GeneralName(GeneralName.uniformResourceIdentifier, CertificateStringConstants.OCSP_URI.txt);
            AccessDescription accessDescription = new AccessDescription(OCSPObjectIdentifiers.id_pkix_ocsp, generalName);
            return new Extension(Extension.authorityInfoAccess, false, new AuthorityInformationAccess(accessDescription).getEncoded());
        } catch (Exception e) {
            throw new CryptoException("Error happened while generating AuthorityInfoAccess extension.", e);
        }
    }

    private PrivateKey getPrivateKeyFromKeyStore(String keystoreFile, String password) throws CryptoException {
        try (FileInputStream fileInputStream = new FileInputStream(keystoreFile)) {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(fileInputStream, password.toCharArray());
            return (PrivateKey) keystore.getKey(CertificateStringConstants.SERVER_DEFAULT_KEY_ALIAS.txt, password.toCharArray());
        } catch (Exception e) {
            throw new CryptoException("Error happened while reading private key from KeyStore.", e);
        }
    }
}
