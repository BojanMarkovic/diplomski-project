package mb160113d.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import mb160113d.enums.CertificateStringConstants;
import mb160113d.exceptions.GuiException;
import mb160113d.services.CryptoService;
import mb160113d.services.GuiService;
import mb160113d.services.UserService;
import org.bouncycastle.asn1.x509.CRLReason;

import java.util.Optional;

public class CertificateRevokeWindow extends Stage {
    private static final String REPLACE_STRING = "CRLReason: ";
    private final GuiService guiService = new GuiService();

    public CertificateRevokeWindow() {
        try {
            guiService.setStage("Welcome to revoking of certificate", this);
            FlowPane flowPane = guiService.generateFlowPane();

            HBox usernameHBox = guiService.generateTextFieldHBox("Username: ");
            HBox passwordHBox = guiService.generatePasswordFieldHBox("Password: ");
            HBox reasonHBox = guiService.generateComboBoxHBox(getReasons(), "Reason:");

            HBox certificateHBox = guiService.generateFileDirChooser("Select Certificate",
                    CertificateStringConstants.CERTIFICATE_FILE_TYPE.txt, false, this);

            Button revokeButton = guiService.generateButton("Do operation", e -> revokeButtonOnAction(certificateHBox, reasonHBox,
                    usernameHBox, passwordHBox));
            Button backButton = guiService.generateButton("Back", e -> backButtonOnAction());

            flowPane.getChildren().addAll(usernameHBox, passwordHBox, reasonHBox, certificateHBox, revokeButton, backButton);
            this.setScene(new Scene(flowPane));
            this.show();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    private ObservableList<String> getReasons() {
        return FXCollections.observableArrayList(
                CRLReason.lookup(CRLReason.unspecified).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.keyCompromise).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.cACompromise).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.affiliationChanged).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.superseded).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.cessationOfOperation).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.certificateHold).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.removeFromCRL).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.privilegeWithdrawn).toString().replace(REPLACE_STRING, ""),
                CRLReason.lookup(CRLReason.aACompromise).toString().replace(REPLACE_STRING, ""));
    }

    private void backButtonOnAction() {
        this.close();
        new WelcomeWindow();
    }

    private void revokeButtonOnAction(HBox certificateHBox, HBox reasonHBox, HBox usernameHBox, HBox passwordHBox) {
        try {
            String certificate = guiService.getTextFieldHBox(certificateHBox).getText();
            String reason = guiService.getValueComboBoxHBox(reasonHBox);
            if (certificate.endsWith(CertificateStringConstants.CERTIFICATE_FILE_TYPE.txt)) {
                CryptoService cryptoService = new CryptoService();
                UserService userService = new UserService();
                Optional<ButtonType> result = guiService.generateAlert(Alert.AlertType.CONFIRMATION, "Confirm",
                        "Do you want to revoke this certificate. \nWith reason: " + reason,
                        cryptoService.getX500NameCertificate(
                                userService.openCertificate(certificate)[0]).toString().replace(",", "\n"));

                if (result.get() == ButtonType.OK) {
                    guiService.generateAlert(Alert.AlertType.INFORMATION, "Server response", "Server:",
                            userService.connectAndSendCertificateRevoke(certificate, reason,
                                    guiService.getTextFieldHBox(usernameHBox).getText(),
                                    guiService.getTextFieldHBox(passwordHBox).getText()));
                }
            } else {
                guiService.generateAlert(Alert.AlertType.INFORMATION, "Input error", "Input error:",
                        "You didn't select valid certificate.");
            }
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }
}
