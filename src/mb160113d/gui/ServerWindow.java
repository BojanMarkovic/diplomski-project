package mb160113d.gui;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import mb160113d.exceptions.GuiException;
import mb160113d.services.GuiService;
import mb160113d.services.ServerService;

public class ServerWindow extends Stage {
    private static final int WIDTH = 50;
    private final GuiService guiService = new GuiService();
    private ServerService serverService;

    public ServerWindow() {
        try {
            guiService.setStage("Welcome to server", this);
            this.setWidth(GuiService.TEXTAREA_WIDTH + WIDTH);
            FlowPane flowPane = guiService.generateFlowPane();

            TextArea textArea = guiService.generateTextArea();
            Button startServer = guiService.generateButton("Start Server", e -> startServer());
            Button stopServer = guiService.generateButton("Stop Server", e -> stopServer());

            flowPane.getChildren().addAll(textArea, startServer, stopServer);
            serverService = new ServerService(textArea);

            this.setScene(new Scene(flowPane));
            this.show();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    public void stopServer() {
        try {
            serverService.stopServer();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    public void startServer() {
        try {
            serverService.startServer();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }
}
