package mb160113d.gui;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import mb160113d.exceptions.GuiException;
import mb160113d.services.GuiService;

public class WelcomeWindow extends Stage {

    public WelcomeWindow() {
        GuiService guiService = new GuiService();
        try {
            guiService.setStage("Welcome to client", this);
            FlowPane flowPane = guiService.generateFlowPane();

            Button certificateRequestButton = guiService.generateButton("Send certificate request", e -> certificateRequestButtonOnAction());
            Button testCertificateButton = guiService.generateButton("Test certificate", e -> testCertificateButtonOnAction());
            Button revokeCertificateButton = guiService.generateButton("Revoke certificate", e -> revokeCertificateButtonOnAction());

            flowPane.getChildren().addAll(certificateRequestButton, testCertificateButton, revokeCertificateButton);
            this.setScene(new Scene(flowPane));
            this.show();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    private void revokeCertificateButtonOnAction() {
        this.close();
        new CertificateRevokeWindow();
    }

    private void testCertificateButtonOnAction() {
        this.close();
        new CertificateTestWindow();
    }

    private void certificateRequestButtonOnAction() {
        this.close();
        new CertificateRequestWindow();
    }
}
