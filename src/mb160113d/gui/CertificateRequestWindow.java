package mb160113d.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import mb160113d.exceptions.GuiException;
import mb160113d.services.CryptoService;
import mb160113d.services.GuiService;
import mb160113d.services.UserService;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;

import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Optional;

public class CertificateRequestWindow extends Stage {
    private static final String KEY_ALGORITHM = "RSA";
    private static final int NUM_OF_HBOXES = 10;
    private final GuiService guiService = new GuiService();
    private final HBox[] hBoxes = new HBox[NUM_OF_HBOXES];

    public CertificateRequestWindow() {
        try {
            guiService.setStage("Send certificate request", this);
            FlowPane flowPane = guiService.generateFlowPane();

            HBox usernameHBox = guiService.generateTextFieldHBox("Username: ");
            HBox passwordHBox = guiService.generatePasswordFieldHBox("Password: ");
            hBoxes[HBoxNames.COMMON_NAME.id] = guiService.generateTextFieldHBox("Common Name (CN):");
            hBoxes[HBoxNames.ORGANIZATION_UNIT.id] = guiService.generateTextFieldHBox("Organization Unit (OU):");
            hBoxes[HBoxNames.ORGANIZATION_NAME.id] = guiService.generateTextFieldHBox("Organization Name (ON):");
            hBoxes[HBoxNames.LOCALITY_NAME.id] = guiService.generateTextFieldHBox("Locality Name (L):");
            hBoxes[HBoxNames.STATE_NAME.id] = guiService.generateTextFieldHBox("State Name (ST):");
            hBoxes[HBoxNames.COUNTRY.id] = guiService.generateTextFieldHBox("Country (C):");
            hBoxes[HBoxNames.PRIVATE_KEY_PASSWORD.id] = guiService.generatePasswordFieldHBox("Private key password:");
            hBoxes[HBoxNames.SIGNATURE_ALGORITHM.id] = guiService.generateComboBoxHBox(getKeyTypes(), "Signature algorithm:");
            hBoxes[HBoxNames.KEY_SIZE.id] = guiService.generateComboBoxHBox(getKeySizes(), "Key size:");
            hBoxes[HBoxNames.OUTPUT_DIRECTORY.id] = guiService.generateFileDirChooser("Select Output Directory", null, true, this);

            Button submitButton = guiService.generateButton("Submit", e -> submitButtonOnAction(usernameHBox, passwordHBox));
            Button backButton = guiService.generateButton("Back", e -> backButtonOnAction());

            flowPane.getChildren().addAll(usernameHBox, passwordHBox);
            for (HBox hBox : hBoxes) {
                flowPane.getChildren().add(hBox);
            }
            flowPane.getChildren().addAll(submitButton, backButton);

            this.setScene(new Scene(flowPane));
            this.show();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    private ObservableList<String> getKeySizes() {
        return FXCollections.observableArrayList(Arrays.asList(KeySizes.SIZE_2048.num, KeySizes.SIZE_4096.num));
    }

    private void backButtonOnAction() {
        this.close();
        new WelcomeWindow();
    }

    private ObservableList<String> getKeyTypes() {
        return FXCollections.observableArrayList(Arrays.asList(
                "SHA512WITHRSAENCRYPTION",
                "SHA512WITHRSA",
                "SHA384WITHRSAENCRYPTION",
                "SHA384WITHRSA",
                "SHA256WITHRSAENCRYPTION",
                "SHA256WITHRSA",
                "MD5WITHRSA",
                "MD5WITHRSAENCRYPTION"));
    }

    private X500Name getSubject() throws Exception {
        X500NameBuilder x500NameBuilder = new X500NameBuilder();
        String country = guiService.getTextFieldHBox(hBoxes[HBoxNames.COUNTRY.id]).getText();
        if (!country.isBlank())
            x500NameBuilder.addRDN(BCStyle.C, country);

        String commonName = guiService.getTextFieldHBox(hBoxes[HBoxNames.COMMON_NAME.id]).getText();
        if (!commonName.isBlank())
            x500NameBuilder.addRDN(BCStyle.CN, commonName);

        String organizationUnit = guiService.getTextFieldHBox(hBoxes[HBoxNames.ORGANIZATION_UNIT.id]).getText();
        if (!organizationUnit.isBlank())
            x500NameBuilder.addRDN(BCStyle.OU, organizationUnit);

        String organizationName = guiService.getTextFieldHBox(hBoxes[HBoxNames.ORGANIZATION_NAME.id]).getText();
        if (!organizationName.isBlank())
            x500NameBuilder.addRDN(BCStyle.O, organizationName);

        String stateName = guiService.getTextFieldHBox(hBoxes[HBoxNames.STATE_NAME.id]).getText();
        if (!stateName.isBlank())
            x500NameBuilder.addRDN(BCStyle.ST, stateName);

        String localityName = guiService.getTextFieldHBox(hBoxes[HBoxNames.LOCALITY_NAME.id]).getText();
        if (!localityName.isBlank())
            x500NameBuilder.addRDN(BCStyle.L, localityName);
        X500Name x500Name = x500NameBuilder.build();
        if (x500Name.toString().isBlank()) {
            throw new Exception("Can't have blank X500Name.\n");
        }
        return x500Name;
    }

    private String writeCertificate(String certificate) {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            CryptoService cryptoService = new CryptoService();
            X509Certificate x509Certificate = cryptoService.getX509Certificate(certificate);

            stringBuilder.append("Received certificate:\n");
            stringBuilder.append("Subject:\n");
            stringBuilder.append(x509Certificate.getSubjectX500Principal().toString().replace(",", "\n"));
            stringBuilder.append("\n").append("Issuer:\n");
            stringBuilder.append(x509Certificate.getIssuerX500Principal().toString().replace(",", "\n"));
            stringBuilder.append("\n").append("Serialnumber:\n");
            stringBuilder.append(x509Certificate.getSerialNumber());
            stringBuilder.append("\n").append("Date valid to:\n");
            stringBuilder.append(x509Certificate.getNotBefore());
            return stringBuilder.toString();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened while writing certificate!",
                        "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
        return null;
    }

    private void submitButtonOnAction(HBox usernameHBox, HBox passwordHBox) {
        try {
            UserService userService = new UserService();
            String privateKeyPassword = guiService.getTextFieldHBox(hBoxes[HBoxNames.PRIVATE_KEY_PASSWORD.id]).getText();
            if (privateKeyPassword.isBlank()) {
                throw new Exception("PrivateKey password must be inputted.\n");
            }
            int keySize = Integer.parseInt(guiService.getValueComboBoxHBox(hBoxes[HBoxNames.KEY_SIZE.id]));
            String signatureAlgorithm = guiService.getValueComboBoxHBox(hBoxes[HBoxNames.SIGNATURE_ALGORITHM.id]);
            X500Name subject = getSubject();
            String outputDirectory = guiService.getTextFieldHBox(hBoxes[HBoxNames.OUTPUT_DIRECTORY.id]).getText();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Key algorithm: ").append(KEY_ALGORITHM).append("\n");
            stringBuilder.append("Key size: ").append(keySize).append("\n");
            stringBuilder.append("Signature algorithm: ").append(signatureAlgorithm).append("\n");
            stringBuilder.append("Subject:\n").append(subject.toString().replace(",", "\n")).append("\n");
            stringBuilder.append("Output directory: ").append(outputDirectory);

            Optional<ButtonType> result = guiService.generateAlert(Alert.AlertType.CONFIRMATION, "Confirm",
                    "Do you want to make CSR and send it to server. \nWith these parameters:",
                    stringBuilder.toString());

            if (result.get() == ButtonType.OK) {
                String temp = userService.connectAndSendCertificateRequest(KEY_ALGORITHM, keySize, signatureAlgorithm,
                        subject, guiService.getTextFieldHBox(usernameHBox).getText(),
                        guiService.getTextFieldHBox(passwordHBox).getText(), privateKeyPassword, outputDirectory);
                if (temp.contains("-----")) {
                    guiService.generateAlert(Alert.AlertType.INFORMATION, "Server response", "Server:",
                            writeCertificate(temp));
                } else {
                    guiService.generateAlert(Alert.AlertType.INFORMATION, "Server response", "Server:", temp);
                }
            }
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    private enum HBoxNames {
        COMMON_NAME(0), ORGANIZATION_UNIT(1), ORGANIZATION_NAME(2), LOCALITY_NAME(3),
        STATE_NAME(4), COUNTRY(5), PRIVATE_KEY_PASSWORD(6), SIGNATURE_ALGORITHM(7),
        KEY_SIZE(8), OUTPUT_DIRECTORY(9);

        public final int id;

        HBoxNames(int i) {
            this.id = i;
        }
    }

    private enum KeySizes {
        SIZE_2048("2048"),
        SIZE_4096("4096");

        public final String num;

        KeySizes(String i) {
            this.num = i;
        }
    }
}
