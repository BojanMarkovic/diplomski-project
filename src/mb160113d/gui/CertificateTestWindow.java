package mb160113d.gui;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import mb160113d.enums.CertificateStringConstants;
import mb160113d.exceptions.GuiException;
import mb160113d.services.CryptoService;
import mb160113d.services.GuiService;
import mb160113d.services.UserService;

import java.util.Optional;

public class CertificateTestWindow extends Stage {
    private final GuiService guiService = new GuiService();

    public CertificateTestWindow() {
        try {
            guiService.setStage("Welcome to testing of certificate", this);
            FlowPane flowPane = guiService.generateFlowPane();

            HBox certificateHBox = guiService.generateFileDirChooser("Select Certificate",
                    CertificateStringConstants.CERTIFICATE_FILE_TYPE.txt, false, this);

            Button testButton = guiService.generateButton("Test", e -> testButtonOnAction(certificateHBox));
            Button backButton = guiService.generateButton("Back", e -> backButtonOnAction());

            flowPane.getChildren().addAll(certificateHBox, testButton, backButton);
            this.setScene(new Scene(flowPane));
            this.show();
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }

    private void backButtonOnAction() {
        this.close();
        new WelcomeWindow();
    }

    private void testButtonOnAction(HBox certificateHBox) {
        try {
            String certificate = guiService.getTextFieldHBox(certificateHBox).getText();
            if (certificate.endsWith(CertificateStringConstants.CERTIFICATE_FILE_TYPE.txt)) {
                CryptoService cryptoService = new CryptoService();
                UserService userService = new UserService();
                Optional<ButtonType> result = guiService.generateAlert(Alert.AlertType.CONFIRMATION, "Confirm",
                        "Do you want to test if this certificate is revoked:",
                        cryptoService.getX500NameCertificate(userService.openCertificate(certificate)[0])
                                .toString().replace(",", "\n"));

                if (result.get() == ButtonType.OK) {
                    guiService.generateAlert(Alert.AlertType.INFORMATION, "Server response", "Server:",
                            userService.connectAndTestCertificate(certificate));
                }
            } else {
                guiService.generateAlert(Alert.AlertType.INFORMATION, "Input error", "Input error:",
                        "You didn't select valid certificate.");
            }
        } catch (Exception e) {
            try {
                guiService.generateAlert(Alert.AlertType.ERROR, "Error", "Error happened!", "Info:\n" + e.getMessage());
            } catch (GuiException guiException) {
                guiException.printStackTrace();
            }
        }
    }
}
