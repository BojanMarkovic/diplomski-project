package mb160113d.enums;

public enum SSLIntConstants {
    PORT_CERTIFICATE(8281),
    PORT_REVOKE(8284),
    PORT_OCSP(8282);

    public final int num;

    SSLIntConstants(int i) {
        this.num = i;
    }
}
