package mb160113d.enums;

public enum SSLStringConstants {
    KEYSTORE("mb160113KeyStore"),
    TRUSTSTORE("mb160113Certificate"),
    KEYSTORE_PASSWORD("test"),
    TRUSTSTORE_PASSWORD("test"),
    KEYSTORE_PROPERTY("javax.net.ssl.keyStore"),
    TRUSTSTORE_PROPERTY("javax.net.ssl.trustStore"),
    KEYSTORE_PASSWORD_PROPERTY("javax.net.ssl.keyStorePassword"),
    TRUSTSTORE_PASSWORD_PROPERTY("javax.net.ssl.trustStorePassword"),
    HOST("localhost"),
    TLS_VERSION("TLSv1.3");

    public final String txt;

    SSLStringConstants(String txt) {
        this.txt = txt;
    }
}
