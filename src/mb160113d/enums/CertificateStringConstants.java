package mb160113d.enums;

public enum CertificateStringConstants {
    CERTIFICATE_TYPE("X.509"),
    PROVIDER("BC"),
    SERVER_DEFAULT_KEY_ALIAS("mb160113"),
    ISSUER_SUBJECT("CN=issuer, O=mb160113Issuer"),
    CERTIFICATE_FILE_TYPE("cer"),
    OCSP_URI("mb160113.diplomski"),
    CERTIFICATE_HEADER("-----BEGIN CERTIFICATE-----"),
    CERTIFICATE_END("-----END CERTIFICATE-----");

    public final String txt;

    CertificateStringConstants(String txt) {
        this.txt = txt;
    }
}
