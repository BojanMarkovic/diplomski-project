package mb160113d;

import javafx.application.Application;
import javafx.stage.Stage;
import mb160113d.gui.WelcomeWindow;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            new WelcomeWindow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
