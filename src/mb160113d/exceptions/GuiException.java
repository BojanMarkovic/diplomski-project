package mb160113d.exceptions;

public class GuiException extends Exception {
    private static final String DEFAULT_MESSAGE = "Gui exception: ";
    private static final String REASON = "\nReason:\n";

    public GuiException(String message, Throwable cause) {
        super(DEFAULT_MESSAGE + message + REASON + cause.getMessage(), cause);
        cause.printStackTrace();
    }
}
