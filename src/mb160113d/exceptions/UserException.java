package mb160113d.exceptions;

public class UserException extends Exception {
    private static final String DEFAULT_MESSAGE = "User exception: ";
    private static final String REASON = "\nReason:\n";

    public UserException(String message) {
        super(DEFAULT_MESSAGE + message);
    }

    public UserException(String message, Throwable cause) {
        super(DEFAULT_MESSAGE + message + REASON + cause.getMessage(), cause);
        cause.printStackTrace();
    }
}
