package mb160113d.exceptions;

public class CryptoException extends Exception {
    private static final String DEFAULT_MESSAGE = "Crypto exception: ";
    private static final String REASON = "\nReason:\n";

    public CryptoException(String message) {
        super(DEFAULT_MESSAGE + message);
    }

    public CryptoException(String message, Throwable cause) {
        super(DEFAULT_MESSAGE + message + REASON + cause.getMessage(), cause);
        cause.printStackTrace();
    }
}
