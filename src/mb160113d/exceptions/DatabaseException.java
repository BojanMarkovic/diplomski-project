package mb160113d.exceptions;

public class DatabaseException extends Exception {
    private static final String DEFAULT_MESSAGE = "Database exception: ";
    private static final String REASON = "\nReason:\n";

    public DatabaseException(String message) {
        super(DEFAULT_MESSAGE + message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(DEFAULT_MESSAGE + message + REASON + cause.getMessage(), cause);
        cause.printStackTrace();
    }
}
