package mb160113d.exceptions;

public class ServerException extends Exception {
    private static final String DEFAULT_MESSAGE = "Server exception: ";
    private static final String REASON = "\nReason:\n";

    public ServerException(String message, Throwable cause) {
        super(DEFAULT_MESSAGE + message + REASON + cause.getMessage(), cause);
        cause.printStackTrace();
    }
}
